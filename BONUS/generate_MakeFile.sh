#!/bin/bash
# generate_MakeFile.sh by BONFILS Antoine, student of POLYTECH GRENOBLE, 
# date of creation: 1st december 2021

# Ce programme permet de générer un Makefile à partir des fichiers .c contenus dans le répertoire.

EXEC=$1 
# et vérification :
if  test -z $EXEC 
	then 
        echo "La prochaine fois que tu execute generate_MakeFile.sh,";
        echo "ajoute le nom de l'executable en argument."; 
	exit; 
fi

echo '#----------------------------------------------------------' > Makefile
echo "# Makefile genereted - `date +%d/%m/%y` - BONFILS Antoine " >> Makefile
echo '#----------------------------------------------------------' >> Makefile
echo ''>> Makefile
echo 'CC = gcc' >> Makefile
echo 'CFLAGS= -Wall -Werror' >> Makefile
echo 'EXEC = '$EXEC >> Makefile
echo 'MAKEFILE = -MM' >> Makefile
echo 'SRC = $(wildcard *.c)' >> Makefile
echo 'OBJ = $(patsubst %.c, %.o, $(SRC))' >> Makefile   #OBJ=SRC:.c=.o
echo 'DEP = $(patsubst %.c, %.dep, $(SRC))' >> Makefile   #DEP=SRC:.c=.dep
echo '.PHONY: clean'>> Makefile
echo ''>> Makefile
#echo OBJETS = `ls *.o` >> Makefile
#echo ''>> Makefile
echo '#-----------------------------------------------------' >> Makefile
echo '# Paramètres à ajuster :' >> Makefile
echo 'LIB = ' >> Makefile
echo 'INCLUDE = ' >> Makefile
echo ''>> Makefile
echo '#-----------------------------------------------------' >> Makefile
echo '' >> Makefile
echo '# Pour informations : ' >> Makefile
echo '# $@    => la cible' >> Makefile
echo '# $^    => toutes les dépendances' >> Makefile
echo '# $<    => la première dépendance' >> Makefile
echo '# -c    => créer le fichier exécutable /  Regle de transformation de *.o en (.x)' >> Makefile
echo '# -o    => créer les fichiers .o / Regle de transformation de *.c en *.o' >> Makefile
echo ''>> Makefile
echo '#------------------------------------------------------'>> Makefile
echo ''>> Makefile
echo 'all:	$(EXEC)'>> Makefile
echo ''>> Makefile
echo '#------------------------------------------------------'>> Makefile
echo ''>> Makefile

# if  test -z OBJETS 
# 	then 
#         echo '$(EXEC): $(OBJ)   #'`ls *.c` >> Makefile
# 	exit; 
#         else
#         echo '$(EXEC): $(OBJ)' >> Makefile
#         exit;
# fi

echo '$(EXEC): $(OBJ)' >> Makefile
echo '	$(CC) $^ -o $@ $(LIB)'>> Makefile
echo ''>> Makefile
echo '#-----------------------------------------------------' >> Makefile
echo ''>> Makefile
echo '$(OBJ): $(DEP)'>> Makefile
echo ''>> Makefile
echo '%.o: %.c '>> Makefile
echo '	$(CC) $(CFLAGS) -c $<'>> Makefile
echo ''>> Makefile
echo '%.dep: %.c'>> Makefile
echo '	$(CC) $(MAKEFILE) $< > $@'>> Makefile
echo ''>> Makefile
echo '#------------------------------------------------------'>> Makefile
echo ''>> Makefile
echo 'mrproper: clean'>> Makefile
echo '	rm -f *.dep'>> Makefile
echo '	rm -f $(EXEC)'>> Makefile
echo ''>> Makefile
echo 'clean:'>> Makefile
echo '	rm -f *.o'>> Makefile
echo ''>> Makefile

#--------------------IDEE--------------------
# - Add option valgrind
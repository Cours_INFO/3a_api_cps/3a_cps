# **Tous les Documents - programmation C et programmation système**

Tous les Documents (Cours, TPs, ...) dans le cadre du cours de programmation C et programmation système à **POLYTECH GRENOBLE**.

**[BONFILS Antoine](https://gitlab.com/AntoineBF)** (INFO3)

***

## **TABLE DE NAVIGATION**

* **[Cours](#table-des-cours)**

* **[TPs](#table-des-tPs)**

***

## **TABLE DES COURS**

| Cours | Traités  |
|:-----:|:--------:|
| [Make](#cours-make-voir-slide) | :heavy_check_mark: |
| [Git](#cours-git-voir-slide) | :heavy_check_mark: |
| [Bibliothèques en C](#cours-bibliothèques-c-voir-slide) | :heavy_check_mark: |
| [Linking](#cours-linking-voir-slide) | :heavy_check_mark: |

***

## **TABLE DES [TPs](./TP/)**

| TDs  | Traités  |
|:----:|:--------:|
| [BiblioCar](./TP/TP_BiblioCar/) | :heavy_check_mark: |
| [Makefile](./TP/TP_Makefile/) | :heavy_check_mark: |
| [Pointeur](./TP/TP_Pointeurs/) | :heavy_minus_sign: |
| [File à priorité](./TP/TP_PointeursFn%2BVarArgs/) | :heavy_check_mark: |
| [Tests - Pointeur](./TP/Tests_Pointeurs/) | :heavy_check_mark: |

***

### Legende:

+ :heavy_check_mark: : trait
+ :heavy_minus_sign: : en cours / non fini
+ :x: : non disponible

***

## Notes de cours

## Cours CPS

### Structure / Liste Chaînée
```
typedef struct fb {
	size_t size;
	struct fb* next;
} LIBRE;
```

---

## Cours Make: VOIR SLIDE

### **Options  de compilation ggc** :
```
- -I : include
- -L : link
- -l : librairies à lier
- -O <number> : optimisation du code
- -Werror : Warning traités comme des erreurs
- -Wall : (Presque) tous les warning possibles sont annoncés
- -D : passer des paramètres
```
### **Variables** :
```
CC = gcc
CFLAGS= -Wall -Werror
EXEC = "nom_executable"
MAKEFILE = -MM
SRC = $(wildcard *.c)
OBJ = $(patsubst %.c, %.o, $(SRC))
DEP = $(patsubst %.c, %.dep, $(SRC))
```
### **Variables Spéciales** :
```
 $@    => la cible
 $^    => toutes les dépendances
 $<    => la première dépendance
 -o    => créer le fichier exécutable /  Regle de transformation de *.o en (.x)
 -c    => créer les fichiers .o / Regle de transformation de *.c en *.o
```
### **Structure du MakeFile** :
```
.PHONY: clean   #PHONY permet d'ôter une ambiguïté si clean est le nom d'un fichier

all:	$(EXEC)

$(EXEC): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

$(OBJ): $(DEP)

%.o: %.c 
	$(CC) $(CFLAGS) -c $<

%.dep: %.c
	$(CC) $(MAKEFILE) $< > $@

mrproper: clean
  rm -f *.dep
  rm -f $(EXEC)

clean:
	rm -f *.o
```

## Cours Git: VOIR SLIDE

- **diff** : comparaison de fichier ligne par ligne
- **patch** : utilise la différence entre deux fichier pour passer d'une version à l'autre
---
- **historique** : un graphe orienté acyclique composé d'un ensemble de versions
- **branche** : est le sous graphe composé de l'ensemble des versions accessible
- **tronc ou branche principale** : la version stable (généralement master ou main avant)
---
- **merge** : fusion des patchs de plusieurs branche (attention aux confilt)
---

Object de type Blob, Tree ou Commit ne sont pas mutables et ne doivent normalement pas être supprimés.

-> chaque commit va "figer" une version de l'arbre

Git utilise SHA-1 -> une fonction de hachage qui génére un hash de 160 bits. (actuellement crackable)

---

### Les Commandes GIT

environ 145 commandes.

Creation d'un dépot :
  - **init** : initilisation d'un depot
  - **clone** : copie d'un dépot existant
  - . . .


Creation d'un dépot serveur : git clone -bare \<URL\> 

pas de fichier historique

- **add** : ajoute dans l'index un fichier à commiter dans son état actuel
- **commit** : enregistre dans le dépot local les modifications qui ont été ajouté à l'index avec la commande add *(-m de commit pour mettre un message)*
---

### Les branches

- **branch** : liste des branches -> * pour celle active
- **branch \<nom\>** : créer une nouvelle branch
- **branch -d nom** : Détruit la branche, sans supprimer les commit
- **branch checkout** : change de branche 

### Small Example
```
git checkout v0 //
git branch -v //affiche les branches avec commits
git branch b1 //crée une branche b1
git branch //affiche les braches
git checkout b1 //se place dans la brache b1
git branch -v //
```

---
### Les merges

---
### Les conflits
```
<<<<<<< HEAD
New version of f1 in master
=======
version of f1 in separed
>>>>>>> separed
```
---
### Les rebases

- volonté de "linéariser l'historique"
- transplante les changement de la branch vers master
- git rebase NomDeLaBranche
- à privilégier en local -> car pas magique 
- git rebase -onto ... ... ...
---
### Retour en arrière

- **revert HEAD^** : fonctionne TOUT LE TEMPS -> fait le patch à l'envert (TRES IMPORTANT) : annuler un commit par un autre commit
- **--ammend** : modifier le dernier commit -> +- seulement en local car reecrit l'historique (Sauf pour les nom sinon)
- **reset** : réécrit l'historique donc faire attention
  - **git reset --hard** : restaure tout
  - **git reset** : juste le depot
  - **git reset --soft** : juste le répertoire de travail 

- **git diff** ...
- **git show** : donne les information sur un commit
  
---
### Dépot distant

- git push : pour envoyer un dépot local dans un dépot distant
- git fetch : charge les mises a jour -> peux faire le merge apres
- git pull : pour faire le fetch et merge en même temps

---
### Modele de travail coopératif

Git permet au dev de gere leurs sources de 4 mainères :
  - Centralized Worlflow
  - Cooperative and decentralized Workflow (gitHub / gitLab)
  - Integration-manager Workflow

---

### Les outils graphiques

- gitk
- tig
- gource
- gitg

---

## Cours Bibliothèques C: VOIR SLIDE

Bibliothèques utilisées:
- update en même temps que le programme

Pourquoi des bibliothèques:
- Réutilisation de codes dans des programme différents
    - Ensemble de fonctions formant un tout cohérent
- Projets indépendants
    - Parfois de très nombreux utilisateurs.
    - exemple extrême mais fort: `libc`.

---
### API et ABI

**API (Application Programming Interface)**
- Interface à destination du programmeur
- fonctions/constantes/variables/etc.
    - ce que le programmeur peut utiliser dans ses propres programmmes

**ABI (Application Binary Interface)**
- Interface du code binaire de la bibliothèque 
    - ce que les autres binaires peuvent utiliser
- principalement constituée de la listes des symboles exportés
- peu d'intérêt avec des bibliothèques statiques
    - l'ABI correspond à l'API utilisé
    - pas/peu de risque d'incompatibilités

**Les liens**
- des modifications de l'API qui entraîne des modifications de l'ABI
- **pas systématiques**
- tous les cas de figures sont possibles

Changer l'API sans toucher à l'ABI
- ajout de macros pour ajouter un préfixes à une fonction tout en redirigeant vers l'ancienne fonction `#define mylib_fct(a) fct(a)`
- suppression d'une constante

Changer l'ABI sans toucher à l'API
- renommer une fonction
- changer la signification de valeurs de constantes. L'API garde des macros des constantes (en mettant la valeur à jour)

---

## Cours Linking: VOIR SLIDE

Utilisation de processus, de piles et de tas.

| Lien Statique | Lien dynamique |
|:------|:------|
| bibliothèque intégrée au binaire du programme | Partage de la bibliothèque entre programme |
| ou distribuée localement avec le programme | disque (code présent une seule fois sur le disque) ou mémoire (partage des pages physiques par l'OS) |
| - mécanismes de l'OS dynamique, mais cas d'usage statique | Update de la bibliothèque indépendamment du/des programmes |
| exemple: DMG (MacOSX) et FlatPack (Linux) | Correction de bugs immédiate dans tous les programmes |

***

## [Script Generateur de MakeFile](./BONUS/generate_MakeFile.sh)

***
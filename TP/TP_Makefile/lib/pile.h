#ifndef PILE
#define PILE
 
typedef struct noeud *pile;

struct noeud{
    int donnee;
    pile prochain;
};

pile pile_vide();

void detruire_pile(pile p);

pile empiler(int donnee, pile p);

int sommet(pile p);

pile depiler(pile p);

int est_pile_vide(pile p);

#endif
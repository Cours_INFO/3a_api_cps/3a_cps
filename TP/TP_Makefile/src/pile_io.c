#include "../lib/pile_io.h"
#include <stdio.h>

/*
   afficher_pile
   description : affiche le contenu d'une pile verticalement
   parametres : pile
   valeur de retour : aucune
   effets de bord : affiche de informations a l'ecran
*/
void afficher_pile(pile p)
{
  pile temp;

  temp = pile_vide();
  /* parcours la pile en la depilant entierement */
  while (!est_pile_vide(p))
    {
      printf("%d\n",sommet(p));
      temp = empiler(sommet(p),temp);
      p = depiler(p);
    }

  /* on la remet en etat en la rempilant dans l'ordre initial */
  while (!est_pile_vide(temp))
    {
      p = empiler(sommet(temp),p);
      temp = depiler(temp);
    }
}

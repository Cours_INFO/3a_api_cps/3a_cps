#include "vecteur_dynamique.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v = malloc(sizeof(struct donnees_vecteur));
    //if(taille >= 0){
    	v->donnees = malloc(taille*sizeof(double));
    	v->taille = taille; //(*v).taille
    //}else{
    //	v = NULL;
    //}
    return v;
}
void liberer_vecteur(vecteur v) {
    free(v->donnees);
    free(v);
}

int est_vecteur_invalide(vecteur v) {
    //int resultat=0;
    //if(&v->donnees[0] < 0) resultat = NULL;
    return (v->donnees == NULL || v == NULL || v->taille < 0);//resultat;
}

double *acces_vecteur(vecteur v, int i) {
    double *resultat = NULL;
    if( i < 0 ){
    	resultat = NULL;
    } else if( i >= 0 && i < v->taille){
    	resultat = &(v->donnees[i]) ;
    }else{
    	v->donnees = (double *) realloc(v->donnees, (i+1)*sizeof(double));
      if( v != NULL){
          v->taille = taille_vecteur(v);
          resultat = &(v->donnees[i]) ;
      }else{
          resultat =  NULL;
      }
    }
    return resultat;
}

int taille_vecteur(vecteur v) {
    return v->taille;
}

#include "vecteur.h"
#include <stdlib.h>
#include <stdio.h>

vecteur allouer_vecteur(int taille) {
    vecteur v ;
    v.taille = taille;
    v.donnees = malloc(taille*sizeof(double));
    return v;
}

void liberer_vecteur(vecteur v) {
    free(v.donnees);
}

int est_vecteur_invalide(vecteur v) {
    int resultat = 0;
    if(&v.donnees[0] == NULL)  resultat = 1;
    return resultat;
}

double *acces_vecteur(vecteur v, int i) {
    //double *resultat = NULL;
    double *resultat = &v.donnees[i];
    // printf("%lf", *resultat);
    return resultat;
}

int taille_vecteur(vecteur v) {
    return v.taille;
}

#include "matrice.h"
#include <stdlib.h>
#include <stdio.h>

matrice allouer_matrice(int l, int c) {
    matrice m;
    m.l = l;
    m.c = c;
    m.donnees = malloc(l*sizeof(double*));   //malloc(c*sizeof(double));
    for(int i = 0; i < l; i++){
      m.donnees[i] = malloc(c*sizeof(double**));
    }
    return m;
}

void liberer_matrice(matrice m) {/*
  int j = nb_lignes_matrice(m);
  int g = nb_colonnes_matrice(m);
  for(int i = 0; i < j; i++){
      for(int h = 0; h < g; h++){
        free(&m.donnees[i][h]);
      }
  }*//*
  for(int i = nb_lignes_matrice(m); i > 0; i--){
    free(m.donnees[i]);
  }
  free(m.donnees);*/
  int j = nb_lignes_matrice(m);
  for(int i = 0; i < j; i++){
    free(m.donnees[i]);
  }
  free(m.donnees);
}

int est_matrice_invalide(matrice m) {
  int resultat = 0;
  if(&m.donnees[0][0] == NULL)  resultat = 1;
  return resultat;
}

double *acces_matrice(matrice m, int i, int j) {
  //double *resultat = NULL;
  double *resultat = &m.donnees[i][j];
  // printf("%lf", *resultat);
  return resultat;
}

int nb_lignes_matrice(matrice m) {
  //printf("%d\n", m.l);
    return m.l;
}

int nb_colonnes_matrice(matrice m) {
    return m.c;
}

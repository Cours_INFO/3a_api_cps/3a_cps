#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
/*
void essai(int nombre, ...)
{
  int i;
  va_list ap;

  va_start(ap,nombre);
  for (i=0;i<nombre;i++)
      printf("%0.1f\n", va_arg(ap,double));
  va_end(ap);
}

int main()
{
  essai(8,5.1,6.4,7.5,9.8,4.6,7.2,2.5,3.4);
  return 0;
}*/


float minFlottant(int nombre, ...)
{
  double min;
  va_list ap;

  va_start(ap, nombre);
  min = va_arg(ap, double);
  double argument;
  for (int i = 1; i < nombre; i++)
  {
    argument = va_arg(ap, double);
    if (argument < min)
    {
      min = argument;
    }
  }

  va_end(ap);
  return (float)min;
}

int main()
{
  float min = minFlottant(5, 12.2, 5.6, 5, 2, 9.2, 3.4, 2.2);
  printf("Plus petit flottant : %0.2f\n", min);
  return 0;
}

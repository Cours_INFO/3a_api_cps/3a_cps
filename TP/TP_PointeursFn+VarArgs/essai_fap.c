#include "fap.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAILLE_BUFFER 128

void aide()
{
  fprintf(stderr,"Aide:\nSaisir l'une des commandes suivantes\n\n");
  fprintf(stderr,"i nombre priorite  :   inserer un nombre avec sa priorite\n");
  fprintf(stderr,"e                  :   extraire le nombre de priorite maximale\n");
  fprintf(stderr,"v                  :   teste si la fap est vide\n");
  fprintf(stderr,"h                  :   afficher cette aide\n");
  fprintf(stderr,"q                  :   quitter ce programme\n");
  fprintf(stderr,"\nNote:\nfap 1 a une politque de priorite dite minimale.\n");
  fprintf(stderr,"fap 2 a une politque de priorite dite maximale.\n");
}

int comp(int el1, int el2)
{
  int res;
  if(el1<el2) {
    res=el1;
  } else {    //if(el1>=el2){
    res=el2;
  }
  return res;
}

int comp_crois(int el1, int el2)
{
  int res;
  if(el1>el2) {
    res=1;
  } else {    //if(el1>=el2){
    res=0;
  }
  //printf("comp_crois : %d\n", res);
  return res;
}

int comp_decrois(int el1, int el2)
{
  int res;
  if(el1<el2) {
    res=1;
  } else {    //if(el1>=el2){
    res=0;
  }
  //printf("comp_decrois : %d\n", res);
  return res;
}

int return_alea(int el1, int el2)
{
  int alea = rand()%2;
  //printf("alea : %d\n", alea);
  return alea;
}

int main()
{
  srand(time(NULL));
  char buffer[TAILLE_BUFFER];
  char commande;
  int nombre,priorite;
  fap f;
  fap f2;

  f = creer_fap_vide(&comp_decrois);
  f2 = creer_fap_vide(&comp_crois);
  aide();
  while (1)
    {
      commande = getchar();
      switch (commande)
        {
        case 'i':
          scanf ("%d",&nombre);
          scanf ("%d",&priorite);
          f = inserer(f,nombre,priorite);
          f2 = inserer(f2,nombre,priorite);
          printf("(%d,%d) a ete insere\n",nombre,priorite);
          break;
        case 'e':
          if (!est_fap_vide(f))
            {
              f = extraire(f,&nombre,&priorite);
              printf("(%d,%d) a ete extrait de la fap 1.\n",nombre,priorite);
            }
          else
              printf("La fap 1 est vide !\n");
/******************************************************************************/
          if (!est_fap_vide(f2))
            {
              f2 = extraire(f2,&nombre,&priorite);
              printf("(%d,%d) a ete extrait de la fap 2.\n",nombre,priorite);
            }
          else
              printf("La fap 2 est vide !\n");

          break;
        case 'v':
          (est_fap_vide(f) == 0) ? printf("File 1 n'est pas vide.") : printf("File 1 est vide.");
          printf("\n");
          (est_fap_vide(f2) == 0) ? printf("File 2 n'est pas vide.") : printf("File 2 est vide.");
          printf("\n");
          break;
        case 'h':
          aide();
          break;
        case 'q':
          detruire_fap(f);
          detruire_fap(f2);
          exit(0);
        default:
          fprintf(stderr,"Commande inconnue !\n");
        }
      /* vide ce qu'il reste de la ligne dans le buffer d'entree */
      fgets(buffer,TAILLE_BUFFER,stdin);
    }
  detruire_fap(f);
  detruire_fap(f2);
  return 0;
}

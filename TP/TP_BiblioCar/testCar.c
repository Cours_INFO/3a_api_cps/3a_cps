#include "car.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void partial_march(car_t v, void *param);

int main(int argc, char **argv)
{

    car_t v1 = create_car(12, 14, "RA 462 HT", "Mr Oui");
    car_t v2 = create_car(15, 123, "DC 352 DY", "Mme Non");

    printf("la couleur de la voiture v1 : %i\n", color_car(v1));
    printf("la couleur de la voiture v2 : %i\n", color_car(v2));

    printf("\n");

    printf("le modele de la voiture v1 : %i\n", modele_car(v1));
    printf("le modele de la voiture v2 : %i\n", modele_car(v2));

    printf("\n");

    printf("la ref de la voiture v1 : %s\n", ref_car(v1));
    printf("la ref de la voiture v2 : %s\n", ref_car(v2));

    printf("\n");

    printf("le proprio de la voiture v1 : %s\n", proprio_car(v1));
    printf("le proprio de la voiture v2 : %s\n\n", proprio_car(v2));

    if(argc > 1){
        iter_car(&partial_march, argv[1]);
    }
    printf("Test partial_march function:\n");
    partial_march(v1, ref_car(v1));

    destroy_car(v1);
    destroy_car(v2);

    return EXIT_SUCCESS;
}

void partial_march(car_t v, void *param)
{
    char *start = (char *)param;
    if (strncmp(ref_car(v), start, strlen(start)) == 0)
    {
        printf("La voiture de %s a pour immatriculation %s qui commence bien par %s\n", proprio_car(v), ref_car(v), start);
    }
}
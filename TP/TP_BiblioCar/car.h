#ifndef CAR_H
#define CAR_H

typedef struct car *car_t;

car_t create_car(int c, int m, char* imm, char* prop);
void destroy_car(car_t v);

int color_car(car_t v);
int modele_car(car_t v);
char* ref_car(car_t v);
char* proprio_car(car_t v);

void changer_proprio_car(car_t v, char* prop);

void iter_car(void (*f)(car_t,void*), char* temp);

#endif
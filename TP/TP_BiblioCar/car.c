#include "car.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct car
{
    int color;
    int modele;
    char *immatriculation;
    char *proprietaire;

    car_t next;
    car_t prev;
};

struct coll{
    car_t tete;
};

static struct coll collection = { .tete = NULL};

void addCollection(car_t v);
void removeCollection(car_t v);


car_t create_car(int c, int m, char *imm, char *prop)
{

    car_t v = malloc(sizeof(struct car));
    if (v != NULL)
    {
        *v = (struct car)
        {
            .color = c,
            .modele = m,
            .immatriculation = strdup(imm),
            .proprietaire = strdup(prop),
        };

        addCollection(v);
    }

    return v;
}

void destroy_car(car_t v){

    removeCollection(v);

    free(v->immatriculation);
    free(v->proprietaire);
    free(v);
}

int color_car(car_t v){
    return v->color;
}
int modele_car(car_t v){
    return v->modele;
}
char *ref_car(car_t v){
    return v->immatriculation;
}
char *proprio_car(car_t v){
    return v->proprietaire;
}

void changer_proprio_car(car_t v, char *prop){
    v->proprietaire = strdup(prop);
}

char *strdup(const char *s)
{
    size_t len = strlen(s) + 1;
    void *new = malloc(len);
    if (new == NULL)
        return NULL;
    return (char *)memcpy(new, s, len);
}

void iter_car(void (*f)(car_t,void*), char* temp){

    car_t acc = collection.tete;

    while(acc != NULL){
        f(acc,(void*)temp);
        acc = acc->next;
    }

}

void addCollection(car_t v){
    if(collection.tete == NULL){
        collection.tete = v;
        v->next = NULL;
        v->prev = NULL;
    }
    else{
        car_t avant = collection.tete;
        while(avant->next != NULL){
            avant = avant->next;
        }
        avant->next = v;
        v->next = NULL;
        v->prev = avant;
    }
}

void removeCollection(car_t v){

    if(collection.tete == v){
        collection.tete = collection.tete->next;

        if(collection.tete != NULL){
            collection.tete->prev = NULL;
        }
    }

    else{
        car_t avant = collection.tete;
        while(avant != NULL){
            if(avant == v){
                avant->prev->next = avant->next;
                avant->next->prev = avant->prev;
                return;
            }
            avant = avant->next;
        }
    }
}

#include <stdlib.h>
#include <stdio.h>
void mystere(void* p, void* a);

int master(int size) {

    int ***a;
    int ***b;
    int **c;
    int ****d;
    int *e;
    int ***t;
    int **x;
    int *y;

    a = b;
    b = &c;
    d = &b;
    printf("\n%p", a);
    e = malloc(size*sizeof(long));
    for(int i=0; i<size;i++)
        e[i]=i;
    c = malloc(sizeof(*c));
    printf("\n%p", e);
    *c = e;
    t = *d;
    x = *t;
    y = *x;

    mystere((void*)y, (void*)a);
    printf("\n%d", *(e+1));
    free(c);
    if(e[1]==42){
        free(e);
        return 0;
    }
    free(e);
    return 1;
}

void mystere(void* p, void* a){
    printf("\n");
    void* valB=p+sizeof(long);
    printf("\n%p", valB);
    printf("\n%p", a);
}

int main(){
    return master(5);
}
#include <stdio.h>
#include <stdlib.h>

int COUNTER = 0;

void afficherTab(int tab[], int taille){
    printf("Numero d'affichage tu tab : %i\n",COUNTER);
    COUNTER++;
    for(int i=0;i<taille;i++){
        printf("tab[%i] : %i | ",i,tab[i]);
    }
    printf("\n");
}

void testTab(){
    int h[3] = {10,20,30};
    int *i;

    i = (int*)malloc(2*sizeof(int));
    afficherTab(i,3);
    printf("A\n");

    i = h;
    //NUM AFF 0
    afficherTab(i,3);
    //NUM AFF 1
    afficherTab(h,3);
    printf("B\n");
    

    i++;
    //NUM AFF 2
    afficherTab(i,3);
    //NUM AFF 3
    afficherTab(h,3);
    printf("C\n");
    

    (*i)++;
    //NUM AFF 4
    afficherTab(i,3);
    //NUM AFF 5
    afficherTab(h,3);
    printf("D\n");
    

    h[2]++;
    //NUM AFF 6
    afficherTab(i,3);
    //NUM AFF 7
    afficherTab(h,3);
    printf("E\n");
    
}


int main(){

    int a = 1; // OUI (NORMAL QUOI)
    //int *b = a; //WARNING INT* -> INT
    //int *c = *a; // NON A N'EST PAS UN POINTEUR DONC *a na pas de sens
    int *d = &a; // OUI -> INT* -> adresse int donc int*
    //int **e = &a; // WARNING INT** -> INT*
    int **f = &d; // OUI -> INT** -> adresse int* donc int**
    int *g = *f; // OUI -> INT * -> valeur int** donc int*

    printf("var de a : %i\n",a);
    printf("l'adresse de a : %p\n",&a);

    //printf("var de b : %i\n",b);
    //printf("pointeur de b : %p\n",&b);

    printf("var de d : %p\n",d);
    printf("l'adresse de d : %p\n",&d);

    //printf("var de e : %p\n",e);

    printf("var de f : %p\n",f);
    printf("l'adresse de f : %p\n",&f);

    printf("var de *f : %p\n",*f);
    printf("l'adresse de *f : %p\n",*&f);
    
    printf("var de g : %p\n",g);
    printf("l'adresse de g : %p\n",&g);
    printf("\n\n\n |||  TEST TAB  |||\n\n\n");


    testTab();/*
    int ***a;
    int ***b;
    int **c;
    int ****d;
    int *e;
    int ***t;
    int **x;
    int *y;
    long i = 0;
    int size = 1;
    a = b;
    b = &c;
    d = &b;
    e = malloc(size*sizeof(long));
    c = malloc(sizeof(*c));
    *c = e;
    t = *d;
    x = *t;
    y = *x;

    //mystere((void*)y, (void*)a);
    free(c);
    if(e[i] == 42){
        free(e);
        return 0;
    }
    free(e);*/
    return 1;

    

    return EXIT_SUCCESS;
}